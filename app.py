import math
import random
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def home():
    return render_template('exercises.html')

@app.route("/exercise1")
def exercise1():
    return render_template('exercise1.html')

@app.route("/exercise2")
def exercise2():
    return render_template('exercise2.html')

@app.route("/exercise3")
def exercise3():
    return render_template('exercise3.html')

@app.route("/exercise4")
def exercise4():
    return render_template('exercise4.html')

@app.route("/exercise5")
def exercise5():
    return render_template('exercise5.html')

@app.route('/exercise1', methods=['POST'])
def exercise1_post():
    dividend = int(request.form['dividend'])
    divisor = int(request.form['divisor'])

    message = ""

    if dividend % 2 == 0:
        message += (f"{dividend} is even.<br>")
    else:
        message += (f"{dividend} is odd.<br>")
    if dividend % 4 == 0:
        message += (f"{dividend} is multiple of 4.<br>")
    if dividend % divisor == 0:
        message += (f"{dividend} is a multiple of {divisor}.")
    
    return f'{message}'

@app.route('/exercise2', methods=['POST'])
def exercise2_post():
    name = request.form['name']
    grade = int(request.form['grade'])

    letter = ""

    if grade < 0 or grade > 100:
        return f'{name} grade must be between 0 and 100, please try again'
    elif grade >= 95:
        letter = "A"
    elif grade >= 80:
        letter = "B"
    elif grade >= 70:
        letter = "C"
    elif grade >= 60:
        letter = "D"
    else:
        letter = "F"

    return f'{name} you scored a: {letter}'

@app.route('/exercise3', methods=['POST'])
def exercise3_post():
    value = int(request.form['value'])

    if value < 0:
        return 'Value cannot be negative'
    sqrt = math.sqrt(value)
    return f'The square root of {value} is: {sqrt}'

@app.route('/exercise4', methods=['POST'])
def exercise4_post():
    number = int(request.form['number'])

    if number < 0 or number > 100:
        return 'Number must be between 0 and 100'
    tries = 1
    magicnum = random.randint(0, 100)
    while number != magicnum:
        tries += 1
        magicnum = random.randint(0, 100)
    return f'It took {tries} randomly generated number to match the number: {number}!'

@app.route('/exercise5', methods=['POST'])
def exercise5_post():
    number = int(request.form['number'])
    output = (f"{number}: 0, 1,")

    num1 = 0
    num2 = 1
    sum = 0

    if number <= 1:
        output = (f"{number}: error... number must be > 1")
    
    while sum <= number:
        sum = num1 + num2
        if sum <= number:
            output += (f" {sum},")

        num1 = num2
        num2 = sum
    return output